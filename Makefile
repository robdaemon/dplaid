ARCH  ?= aarch64
BOARD ?= raspi3

ROOT_SOURCE_DIR = ./source
BUILD_PATH      = ./build

ASM_SOURCES =  $(shell find $(ROOT_SOURCE_DIR)/string/$(ARCH) -name "*.S")
ASM_SOURCES += $(shell find $(ROOT_SOURCE_DIR)/arch/$(ARCH) -name "*.S")
ASM_SOURCES += $(shell find $(ROOT_SOURCE_DIR)/board/$(BOARD) -name "*.S")
ASM_OBJECTS =  $(patsubst %.S,$(BUILD_PATH)/%.o,	$(ASM_SOURCES))

D_SOURCES   = $(shell find $(ROOT_SOURCE_DIR) -name "*.d")

TRIPLE   = aarch64-none-elf
DCFLAGS  = --mtriple=$(TRIPLE) -defaultlib=
LDFLAGS  = -nostdlib -O0 -ffreestanding  -Wl,--gc-sections -ggdb
CFLAGS   = -g
LIBS     = -lgcc
DC       = ldc2
LD       = $(TRIPLE)-gcc
DUBFLAGS = -v

.PHONY : clean run

all: plaid

plaid: $(ASM_OBJECTS) $(D_SOURCES)
	CC=aarch64-none-elf-gcc dub build $(DUBFLAGS) --arch=$(TRIPLE) --compiler=ldc2 --config=$(ARCH)-$(BOARD)

$(BUILD_PATH)/%.o : %.S
	@mkdir -p $(dir $(OUTPUT)$@)
	$(TRIPLE)-gcc $(CFLAGS) -c $< -o $(OUTPUT)$@

clean:
	dub clean || true
	rm -f plaid $(ASM_OBJECTS)

run:
	qemu-system-aarch64 -M raspi3b -kernel plaid -serial mon:stdio -nographic $(QEMU_ARGS)
