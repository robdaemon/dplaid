module board.raspi3.irq;

import core.volatile;

import console;

import board.raspi3.timer;

enum uint IRQ_BASIC_PENDING = 0x0000B200;
enum uint IRQ_PENDING_1 = 0x0000B204;
enum uint IRQ_PENDING_2 = 0x0000B208;
enum uint FIQ_CONTROL = 0x0000B20C;
enum uint ENABLE_IRQS_1 = 0x0000B210;
enum uint ENABLE_IRQS_2 = 0x0000B214;
enum uint ENABLE_BASIC_IRQS = 0x0000B218;
enum uint DISABLE_IRQS_1 = 0x0000B21C;
enum uint DISABLE_IRQS_2 = 0x0000B220;
enum uint DISABLE_BASIC_IRQS = 0x0000B224;

enum uint SYSTEM_TIMER_IRQ_0 = 1 << 0;
enum uint SYSTEM_TIMER_IRQ_1 = 1 << 1;
enum uint SYSTEM_TIMER_IRQ_2 = 1 << 2;
enum uint SYSTEM_TIMER_IRQ_3 = 1 << 3;

immutable(const(char*))[] entry_error_messages = [
    "SYNC_INVALID_EL1t", "IRQ_INVALID_EL1t",
    "FIQ_INVALID_EL1t", "ERROR_INVALID_EL1T",

    "SYNC_INVALID_EL1h", "IRQ_INVALID_EL1h",
    "FIQ_INVALID_EL1h", "ERROR_INVALID_EL1h",

    "SYNC_INVALID_EL0_64", "IRQ_INVALID_EL0_64",
    "FIQ_INVALID_EL0_64", "ERROR_INVALID_EL0_64",

    "SYNC_INVALID_EL0_32", "IRQ_INVALID_EL0_32",
    "FIQ_INVALID_EL0_32", "ERROR_INVALID_EL0_32"
];

shared uint _base_addr = 0x0;

extern (C) void irq_vector_init();
extern (C) void enable_irq();
extern (C) void disable_irq();

void irq_init(uint base_addr)
{
    _base_addr = base_addr;

    puts("irq init\n");

    puts("irq vector init\n");
    irq_vector_init();
}

extern (C) void handle_irq()
{
    uint irq = volatileLoad(cast(uint*)(_base_addr + IRQ_PENDING_1));
    switch (irq)
    {
    case (SYSTEM_TIMER_IRQ_1):
        handle_timer_irq();
        break;
    default:
        // , irq
        puts("Unknown pending irq: %x\n");
    }
}

extern (C) void show_invalid_entry_message(int type, ulong esr, ulong addr)
{
    puts(entry_error_messages[type]);
    puts(" ");
    puts("\n");
}
