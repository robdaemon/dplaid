module board.raspi3;

import board.raspi3.irq;
import board.raspi3.timer;

import drivers.bcm2xxx.gpio;
import drivers.bcm2xxx.mailbox;
import drivers.uart.pl011;

import console;

enum uint MMIO_BASE = 0x3F000000;

void board_init()
{
    mailbox_init(MMIO_BASE);
    uart_init(MMIO_BASE);
    puts("board init\n");

    irq_init(MMIO_BASE);
    timer_init(MMIO_BASE);

    enable_irq();
}
