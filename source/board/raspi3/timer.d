module board.raspi3.timer;

import core.volatile;

import board.raspi3.irq;

import console;

enum uint TIMER_CS = 0x00003000;
enum uint TIMER_CLO = 0x00003004;
enum uint TIMER_CHI = 0x00003008;
enum uint TIMER_C0 = 0x0000300C;
enum uint TIMER_C1 = 0x00003010;
enum uint TIMER_C2 = 0x00003014;
enum uint TIMER_C3 = 0x00003018;

enum uint TIMER_CS_M0 = (1 << 0);
enum uint TIMER_CS_M1 = (1 << 1);
enum uint TIMER_CS_M2 = (1 << 2);
enum uint TIMER_CS_M3 = (1 << 3);

const uint interval = 200_000;

uint curVal = 0;

shared uint _base_addr = 0x0;

void timer_init(uint base_addr)
{
    _base_addr = base_addr;

    puts("timer init\n");

    curVal = volatileLoad(cast(uint*)(_base_addr + TIMER_CLO));
    curVal += interval;
    volatileStore(cast(uint*)(_base_addr + TIMER_C1), curVal);

    volatileStore(cast(uint*)(_base_addr + ENABLE_IRQS_1), SYSTEM_TIMER_IRQ_1);
}

void handle_timer_irq()
{
    curVal += interval;
    volatileStore(cast(uint*)(_base_addr + TIMER_C1), curVal);
    volatileStore(cast(uint*)(_base_addr + TIMER_CS), TIMER_CS_M1);
    puts("ping!\n");
}
