module console;

version (raspi3)
{
    import drivers.uart.pl011;

    alias getc = drivers.uart.pl011.uart_getc;
    alias putc = drivers.uart.pl011.uart_putc;
    alias puts = drivers.uart.pl011.uart_puts;
}
