/*
	This code is based upon the following:

    Part of the Raspberry-Pi Bare Metal Tutorials
    https://www.valvers.com/rpi/bare-metal/
    Copyright (c) 2013-2018, Brian Sidebotham

    This software is licensed under the MIT License.
    Please see the LICENSE file included with this software.
*/

module drivers.bcm2xxx.mailbox;

import ldc.llvmasm;

import core.volatile;

import libk;

enum uint MAILBOX_BASE = 0xB880;

enum MailboxTag : uint
{
	/* Videocore */
	TAG_GET_FIRMWARE_VERSION = 0x1,

	/* Hardware */
	TAG_GET_BOARD_MODEL = 0x10001,
	TAG_GET_BOARD_REVISION,
	TAG_GET_BOARD_MAC_ADDRESS,
	TAG_GET_BOARD_SERIAL,
	TAG_GET_ARM_MEMORY,
	TAG_GET_VC_MEMORY,
	TAG_GET_CLOCKS,

	/* Config */
	TAG_GET_COMMAND_LINE = 0x50001,

	/* Shared resource management */
	TAG_GET_DMA_CHANNELS = 0x60001,

	/* Power */
	TAG_GET_POWER_STATE = 0x20001,
	TAG_GET_TIMING,
	TAG_SET_POWER_STATE = 0x28001,

	/* Clocks */
	TAG_GET_CLOCK_STATE = 0x30001,
	TAG_SET_CLOCK_STATE = 0x38001,
	TAG_GET_CLOCK_RATE = 0x30002,
	TAG_SET_CLOCK_RATE = 0x38002,
	TAG_GET_MAX_CLOCK_RATE = 0x30004,
	TAG_GET_MIN_CLOCK_RATE = 0x30007,
	TAG_GET_TURBO = 0x30009,
	TAG_SET_TURBO = 0x38009,

	/* Voltage */
	TAG_GET_VOLTAGE = 0x30003,
	TAG_SET_VOLTAGE = 0x38003,
	TAG_GET_MAX_VOLTAGE = 0x30005,
	TAG_GET_MIN_VOLTAGE = 0x30008,
	TAG_GET_TEMPERATURE = 0x30006,
	TAG_GET_MAX_TEMPERATURE = 0x3000A,
	TAG_ALLOCATE_MEMORY = 0x3000C,
	TAG_LOCK_MEMORY = 0x3000D,
	TAG_UNLOCK_MEMORY = 0x3000E,
	TAG_RELEASE_MEMORY = 0x3000F,
	TAG_EXECUTE_CODE = 0x30010,
	TAG_GET_DISPMANX_MEM_HANDLE = 0x30014,
	TAG_GET_EDID_BLOCK = 0x30020,

	/* Framebuffer */
	TAG_ALLOCATE_BUFFER = 0x40001,
	TAG_RELEASE_BUFFER = 0x48001,
	TAG_BLANK_SCREEN = 0x40002,
	TAG_GET_PHYSICAL_SIZE = 0x40003,
	TAG_TEST_PHYSICAL_SIZE = 0x44003,
	TAG_SET_PHYSICAL_SIZE = 0x48003,
	TAG_GET_VIRTUAL_SIZE = 0x40004,
	TAG_TEST_VIRTUAL_SIZE = 0x44004,
	TAG_SET_VIRTUAL_SIZE = 0x48004,
	TAG_GET_DEPTH = 0x40005,
	TAG_TEST_DEPTH = 0x44005,
	TAG_SET_DEPTH = 0x48005,
	TAG_GET_PIXEL_ORDER = 0x40006,
	TAG_TEST_PIXEL_ORDER = 0x44006,
	TAG_SET_PIXEL_ORDER = 0x48006,
	TAG_GET_ALPHA_MODE = 0x40007,
	TAG_TEST_ALPHA_MODE = 0x44007,
	TAG_SET_ALPHA_MODE = 0x48007,
	TAG_GET_PITCH = 0x40008,
	TAG_GET_VIRTUAL_OFFSET = 0x40009,
	TAG_TEST_VIRTUAL_OFFSET = 0x44009,
	TAG_SET_VIRTUAL_OFFSET = 0x48009,
	TAG_GET_OVERSCAN = 0x4000A,
	TAG_TEST_OVERSCAN = 0x4400A,
	TAG_SET_OVERSCAN = 0x4800A,
	TAG_GET_PALETTE = 0x4000B,
	TAG_TEST_PALETTE = 0x4400B,
	TAG_SET_PALETTE = 0x4800B,
	TAG_SET_CURSOR_INFO = 0x8011,
	TAG_SET_CURSOR_STATE = 0x8010,
}

enum TagState : uint
{
	TAG_STATE_REQUEST = 0,
	TAG_STATE_RESPONSE = 1,
}

enum TagBufferOffset : uint
{
	PT_OSIZE = 0,
	PT_OREQUEST_OR_RESPONSE = 1,
}

enum TagOffset : uint
{
	T_OIDENT = 0,
	T_OVALUE_SIZE = 1,
	T_ORESPONSE = 2,
	T_OVALUE = 3,
}

struct MailboxProperty
{
	uint tag;
	uint byte_length;
	union
	{
		int value32;
		char[256] buffer_8;
		int[64] buffer_32;
	}
}

enum MailboxChannel : uint
{
	MB0_POWER_MANAGEMENT = 0,
	MB0_FRAMEBUFFER,
	MB0_VIRTUAL_UART,
	MB0_VCHIQ,
	MB0_LEDS,
	MB0_BUTTONS,
	MB0_TOUCHSCREEN,
	MB0_UNUSED,
	MB0_TAGS_ARM_TO_VC,
	MB0_TAGS_VC_TO_ARM,
}

enum MailboxStatusBits : uint
{
	ARM_MS_FULL = 0x80000000,
	ARM_MS_EMPTY = 0x40000000,
	ARM_MS_LEVEL = 0x400000FF,
}

struct Mailbox
{
	uint read;
	uint[((0x90 - 0x80) / 4) - 1] reserved1;
	uint poll;
	uint sender;
	uint status;
	uint configuration;
	uint write;
}

__gshared Mailbox* mailbox = null;

align(16) static int[8192] pt;
static int pt_index = 0;

void property_init()
{
	// Fill in the size on-the-fly
	pt[TagBufferOffset.PT_OSIZE] = 12;

	// Process request (All other values are reserved!)
	pt[TagBufferOffset.PT_OREQUEST_OR_RESPONSE] = 0;

	// First available data slot
	pt_index = 2;

	// null tag to terminate tag list
	pt[pt_index] = 0;
}

void property_add_tag(uint[] tags)
{
	foreach (i, ref tag; tags)
	{
		switch (tag)
		{
		case MailboxTag.TAG_GET_FIRMWARE_VERSION:
		case MailboxTag.TAG_GET_BOARD_MODEL:
		case MailboxTag.TAG_GET_BOARD_REVISION:
		case MailboxTag.TAG_GET_BOARD_MAC_ADDRESS:
		case MailboxTag.TAG_GET_BOARD_SERIAL:
		case MailboxTag.TAG_GET_ARM_MEMORY:
		case MailboxTag.TAG_GET_VC_MEMORY:
		case MailboxTag.TAG_GET_DMA_CHANNELS:
			/* Provide an 8-byte buffer for the response */
			pt[pt_index++] = 8;
			pt[pt_index++] = 0; /* Request */
			pt_index += 2;
			break;

		case MailboxTag.TAG_GET_CLOCKS:
		case MailboxTag.TAG_GET_COMMAND_LINE:
			/* Provide a 256-byte buffer */
			pt[pt_index++] = 256;
			pt[pt_index++] = 0; /* Request */
			pt_index += 256 >> 2;
			break;

		case MailboxTag.TAG_ALLOCATE_BUFFER:
		case MailboxTag.TAG_GET_MAX_CLOCK_RATE:
		case MailboxTag.TAG_GET_MIN_CLOCK_RATE:
		case MailboxTag.TAG_GET_CLOCK_RATE:
			pt[pt_index++] = 8;
			pt[pt_index++] = 0; /* Request */
			pt[pt_index++] = tag;
			pt[pt_index++] = 0;
			break;

		case MailboxTag.TAG_SET_CLOCK_RATE:
			pt[pt_index++] = 12;
			pt[pt_index++] = 0; /* Request */
			pt[pt_index++] = tag; /* Clock ID */
			pt[pt_index++] = tag; /* Rate (in Hz) */
			pt[pt_index++] = tag; /* Skip turbo setting if == 1 */
			break;

		case MailboxTag.TAG_GET_PHYSICAL_SIZE:
		case MailboxTag.TAG_SET_PHYSICAL_SIZE:
		case MailboxTag.TAG_TEST_PHYSICAL_SIZE:
		case MailboxTag.TAG_GET_VIRTUAL_SIZE:
		case MailboxTag.TAG_SET_VIRTUAL_SIZE:
		case MailboxTag.TAG_TEST_VIRTUAL_SIZE:
		case MailboxTag.TAG_GET_VIRTUAL_OFFSET:
		case MailboxTag.TAG_SET_VIRTUAL_OFFSET:
			pt[pt_index++] = 8;
			pt[pt_index++] = 0; /* Request */

			if ((tag == MailboxTag.TAG_SET_PHYSICAL_SIZE) ||
				(tag == MailboxTag.TAG_SET_VIRTUAL_SIZE) ||
				(tag == MailboxTag.TAG_SET_VIRTUAL_OFFSET) ||
				(tag == MailboxTag.TAG_TEST_PHYSICAL_SIZE) ||
				(tag == MailboxTag.TAG_TEST_VIRTUAL_SIZE))
			{
				pt[pt_index++] = tag; /* Width */
				pt[pt_index++] = tag; /* Height */
			}
			else
			{
				pt_index += 2;
			}
			break;

		case MailboxTag.TAG_GET_ALPHA_MODE:
		case MailboxTag.TAG_SET_ALPHA_MODE:
		case MailboxTag.TAG_GET_DEPTH:
		case MailboxTag.TAG_SET_DEPTH:
		case MailboxTag.TAG_GET_PIXEL_ORDER:
		case MailboxTag.TAG_SET_PIXEL_ORDER:
		case MailboxTag.TAG_GET_PITCH:
			pt[pt_index++] = 4;
			pt[pt_index++] = 0; /* Request */

			if ((tag == MailboxTag.TAG_SET_DEPTH) ||
				(tag == MailboxTag.TAG_SET_PIXEL_ORDER) ||
				(tag == MailboxTag.TAG_SET_ALPHA_MODE))
			{
				/* Colour Depth, bits-per-pixel \ Pixel Order State */
				pt[pt_index++] = tag;
			}
			else
			{
				pt_index += 1;
			}
			break;

		case MailboxTag.TAG_GET_OVERSCAN:
		case MailboxTag.TAG_SET_OVERSCAN:
			pt[pt_index++] = 16;
			pt[pt_index++] = 0; /* Request */

			if ((tag == MailboxTag.TAG_SET_OVERSCAN))
			{
				pt[pt_index++] = tag; /* Top pixels */
				pt[pt_index++] = tag; /* Bottom pixels */
				pt[pt_index++] = tag; /* Left pixels */
				pt[pt_index++] = tag; /* Right pixels */
			}
			else
			{
				pt_index += 4;
			}
			break;

		default:
			/* Unsupported tags, just remove the tag from the list */
			pt_index--;
			break;
		}
	}

	/* Make sure the tags are 0 terminated to end the list and update the buffer size */
	pt[pt_index] = 0;
}

int property_process()
{
	int result;

	/* Fill in the size of the buffer */
	pt[TagBufferOffset.PT_OSIZE] = (pt_index + 1) << 2;
	pt[TagBufferOffset.PT_OREQUEST_OR_RESPONSE] = 0;

	mailbox_write(MailboxChannel.MB0_TAGS_ARM_TO_VC, cast(uint)&pt);

	result = mailbox_read(MailboxChannel.MB0_TAGS_ARM_TO_VC);

	return result;
}

MailboxProperty* property_get(MailboxTag tag)
{
	static MailboxProperty property;
	int* tag_buffer = null;

	property.tag = tag;

	/* Get the tag from the buffer. Start at the first tag position  */
	int index = 2;

	while (index < (pt[TagBufferOffset.PT_OSIZE] >> 2))
	{
		/* printf( "Test Tag: [%d] %8.8X\r\n", index, pt[index] ); */
		if (pt[index] == tag)
		{
			tag_buffer = &pt[index];
			break;
		}

		/* Progress to the next tag if we haven't yet discovered the tag */
		index += (pt[index + 1] >> 2) + 3;
	}

	/* Return null of the property tag cannot be found in the buffer */
	if (tag_buffer == null)
		return null;

	/* Return the required data */
	property.byte_length = tag_buffer[TagOffset.T_ORESPONSE] & 0xFFFF;
	memcpy(&property.buffer_8, &tag_buffer[TagOffset.T_OVALUE], property.byte_length);

	return &property;
}

void mailbox_init(uint base_address)
{
	mailbox = cast(Mailbox*)cast(uint*)(base_address + MAILBOX_BASE);
}

void mailbox_write(MailboxChannel channel, int value)
{
	/* For information about accessing mailboxes, see:
       https://github.com/raspberrypi/firmware/wiki/Accessing-mailboxes */

	/* Add the channel number into the lower 4 bits */
	value &= ~(0xF);
	value |= channel;

	/* Wait until the mailbox becomes available and then write to the mailbox
       channel */
	while ((mailbox.status & MailboxStatusBits.ARM_MS_FULL) != 0)
	{
	}

	/* Write the modified value + channel number into the write register */
	mailbox.write = value;
}

int mailbox_read(MailboxChannel channel)
{
	/* For information about accessing mailboxes, see:
       https://github.com/raspberrypi/firmware/wiki/Accessing-mailboxes */
	int value = -1;

	/* Keep reading the register until the desired channel gives us a value */
	while ((value & 0xF) != channel)
	{
		/* Wait while the mailbox is empty because otherwise there's no value
           to read! */
		while (mailbox.status & MailboxStatusBits.ARM_MS_EMPTY)
		{
		}

		/* Extract the value from the Read register of the mailbox. The value
           is actually in the upper 28 bits */
		value = mailbox.read;
	}

	/* Return just the value (the upper 28-bits) */
	return value >> 4;
}
