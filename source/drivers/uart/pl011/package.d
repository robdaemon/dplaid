module drivers.uart.pl011;

import ldc.llvmasm;

import core.volatile;

import drivers.bcm2xxx.gpio;
import drivers.bcm2xxx.mailbox;

enum uint UART0_DR = 0x00201000;
enum uint UART0_FR = 0x00201018;
enum uint UART0_IBRD = 0x00201024;
enum uint UART0_FBRD = 0x00201028;
enum uint UART0_LCRH = 0x0020102C;
enum uint UART0_CR = 0x00201030;
enum uint UART0_IMSC = 0x00201038;
enum uint UART0_ICR = 0x00201044;

shared uint UART_BASE = 0x0;

void uart_init(uint base_address)
{
    UART_BASE = base_address;

    // turn off UART0
    volatileStore(cast(uint*)(UART_BASE + UART0_CR), 0u);

    // setup clock
    property_init();
    static tags = [MailboxTag.TAG_SET_CLOCK_RATE, 12, 8, 2, 4_000_000, 0];
    property_add_tag(tags);
    property_process();

    uint r;

    r = volatileLoad(cast(uint*)(UART_BASE + GPFSEL1));
    r &= ~((7 << 12) | (7 << 15)); // gpio14, gpio15
    r |= (4 << 12) | (4 << 15); // alt0

    volatileStore(cast(uint*)(UART_BASE + GPFSEL1), r);
    volatileStore(cast(uint*)(UART_BASE + GPPUD), 0u); // enable pins 14 and 15
    r = 150;
    while (r--)
    {
        __asm("nop", "");
    }

    volatileStore(cast(uint*)(UART_BASE + GPPUDCLK0), (1 << 14) | (1 << 15));
    r = 150;
    while (r--)
    {
        __asm("nop", "");
    }

    volatileStore(cast(uint*)(UART_BASE + GPPUDCLK0), 0u); // flush GPIO setup

    volatileStore(cast(uint*)(UART_BASE + UART0_ICR), 0x7FF); // clear interrupts
    volatileStore(cast(uint*)(UART_BASE + UART0_IBRD), 2); // 115200 baud
    volatileStore(cast(uint*)(UART_BASE + UART0_FBRD), 0xB);
    volatileStore(cast(uint*)(UART_BASE + UART0_LCRH), 0x7 << 4); // 8n1, enable FIFOs
    volatileStore(cast(uint*)(UART_BASE + UART0_CR), 0x301); // enable Tx, Rx, UART
}

char uart_getc()
{
    char r;
    /* wait until something is in the buffer */
    do
    {
        __asm("nop", "");
    }
    while (volatileLoad(cast(uint*)(UART_BASE + UART0_FR)) & 0x10);

    /* read it and return */
    r = cast(char) volatileLoad(cast(uint*)(UART_BASE + UART0_DR));

    /* convert carrige return to newline */
    return r == '\r' ? '\n' : r;
}

void uart_putc(char ch)
{
    do
    {
        __asm("nop", "");
    }
    while (volatileLoad(cast(uint*)(UART_BASE + UART0_FR)) & 0x20);

    volatileStore(cast(uint*)(UART_BASE + UART0_DR), ch);
}

void uart_puts(const(char*) str)
{
    int i = 0;
    while (str[i] != 0)
    {
        uart_putc(str[i]);
        i++;
    }
}

void uart_puts(immutable(char*) str)
{
    int i = 0;
    while (str[i] != 0)
    {
        uart_putc(str[i]);
        i++;
    }
}
