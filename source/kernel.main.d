module src.kernel.main;

import ldc.llvmasm;

version (AArch64)
{
    import arch.aarch64;
}

version (raspi3)
{
    import board.raspi3;

    import drivers.uart.pl011;
}

import console;

// satisfy the linker
extern (C) void* _Dmodule_ref = null;

extern (C) void __assert(const(char)* exp, const(char)* file, uint line)
{
    puts("DEBUG Assertion ");
    puts(exp);
    puts(" at ");
    puts(file);
    puts("\n");
}

extern (C) void kmain()
{
    arch_init();
    board_init();

    puts("Plaid kernel startup\n");

    for (;;)
    {

    }
}
