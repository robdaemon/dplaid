module libk;

version (AArch64)
{
    extern (C) void __memcpy_aarch64(void* src, void* dest, size_t size);

    alias memcpy = __memcpy_aarch64;
}
